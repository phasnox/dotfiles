## To convert to json

```
qmk json2c phasnox.json > ~/src/qmk_firmware/keyboards/crkbd/keymaps/phasnox/phasnox_keymap.h
```

```
qmk json2c phasnox.json > ~/src/qmk_manna_harbour/keyboards/crkbd/keymaps/manna-harbour_phasnox/phasnox_keymap.h
```

## To compile/flash

Old PCB
```
cd qmk_firmware
qmk compile -kb crkbd -km phasnox &&  qmk flash -kb crkbd -km phasnox -bl dfu
```

Newer hotswapable

```
cd qmk_manna_harbour
qmk compile -kb crkbd -km manna-harbour_phasnox:dfu-split-left
qmk compile -kb crkbd -km manna-harbour_phasnox:dfu-split-right

```

This no longer needed:
```
qmk flash -kb crkbd -km manna-harbour_phasnox:dfu-split-right -bl dfu
```
