#!/bin/bash

echo ""
echo "Building right side.."
cd /home/phasnox/src/corne/zmk/zmk/app
west build --pristine -b nice_nano -- -DSHIELD=corne_right -DZMK_CONFIG="/home/phasnox/src/corne/zmk/zmk-config/config/"
cp ~/src/corne/zmk/zmk/app/build/zephyr/zmk.uf2 ~/src/corne/zmk/firmware/corne_right_nice_nano.uf2

echo ""
echo "Building left side.."
west build --pristine -b nice_nano -- -DSHIELD=corne_left -DZMK_CONFIG="/home/phasnox/src/corne/zmk/zmk-config/config/"
cp ~/src/corne/zmk/zmk/app/build/zephyr/zmk.uf2 ~/src/corne/zmk/firmware/corne_left_nice_nano.uf2

echo ""
echo "To flash: "
echo "-------------------------------"
echo "cp ~/src/corne/zmk/firmware/corne_right_nice_nano.uf2 /run/media/phasnox/NICENANO/CURRENT.UF2"
echo "cp ~/src/corne/zmk/firmware/corne_left_nice_nano.uf2 /run/media/phasnox/NICENANO/CURRENT.UF2"
echo "-------------------------------"
