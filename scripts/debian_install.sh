#!/bin/bash
sudo apt install -y apt-transport-https curl

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update -y

sudo apt install -y \
    fish vim git dkms python3 python3-venv vim-gtk3 neovim brave-browser ripgrep\
    tmux util-linux util-linux-locales docker.io docker-compose silversearcher-ag fzf\
    elixir erlang-p1-pkix erlang-public-key libsodium-dev\
    make clang sysconftool automake kbd\
    x11-utils fontconfig libxft-dev libxinerama-dev stow\
    tor torbrowser-launcher zsh bluez\
    virtinst qemu-system qemu-system-gui virt-viewer virt-manager sway neofetch\
    figlet toilet tree rbenv direnv gnupg
