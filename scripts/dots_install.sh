#!/bin/sh
echo $(pwd)

stow -vt ~ -d dots $(ls dots) --ignore "bash|fish|zsh"
mv ~/.bashrc ~/.bashrc.bkup
mv ~/.zshrc ~/.zshrc.bkup
stow -vt ~ -d dots/shells $(ls dots/shells)
