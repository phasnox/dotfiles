#!/bin/bash
	
# Set source and target directories
dotfiles_fonts_dir="$( cd $HOME/.dotfiles/fonts/ && pwd )/"

if [[ ! -d $dotfiles_fonts_dir ]];
then
  echo "[-] ERROR. $dotfiles_fonts_dir not found."
  exit 1
fi

find_command="find \"$dotfiles_fonts_dir\" \( -name '*.[o,t]tf' -or -name '*.pcf.gz' \) -type f -print0"
if [[ $(uname) == 'Darwin' ]]; then
  # MacOS
  font_dir="$HOME/Library/Fonts"
else
  # Linux
  font_dir="$HOME/.fonts"
  mkdir -p "$font_dir"
fi

# Copy all fonts to user fonts directory
#echo "Copying fonts..."
eval "$find_command" | xargs -0 -I % cp "%" "$font_dir/"

echo "Copying fonts..."
while read line; do
    (cd $font_dir && wget -N $line)
done < $dotfiles_fonts_dir/fonts.txt

# Reset font cache on Linux
if command -v fc-cache @>/dev/null ; then
    echo "Resetting font cache, this may take a moment..."
    fc-cache -f "$font_dir"
fi

echo "All fonts installed to $font_dir"
