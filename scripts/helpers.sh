# Helpers functions

# Get the os name
function get_distro() {
    cat /etc/os-release | grep "^ID=" | tr -d "ID="
}
