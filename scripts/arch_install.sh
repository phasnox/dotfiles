#!/bin/sh

sudo pacman -Syu tree direnv zsh stow fish vim python i3-gaps dmenu xfce4\
  xfce4-goodies i3-wm ripgrep the_silver_searcher bash-completion shellcheck\
  bash-language-server alacritty kitty xclip docker docker-compose tmux fzf\
  neovim rofi inotify-tools udisks2 udiskie

sudo pacman -S \
  pamixer blueman dunst maim network-manager-applet neofetch sox\
  polybar trayer arandr xorg-xev nitrogen xorg-xbacklight picom feh

# Install paru
sudo pacman -S --needed base-devel

if [[ ! -d ~/src/arch/paru/ ]];
then
  mkdir -p ~/src/arch/
  git clone https://aur.archlinux.org/paru.git ~/src/arch/paru/
  (cd ~/src/arch/paru && makepkg -si)
fi

paru -Syu rofi-file-browser-extended-git rofi-power-menu

rehash

xdg-mime default firedragon.desktop x-scheme-handler/https
xdg-mime default firedragon.desktop x-scheme-handler/http
xdg-settings set default-web-browser firedragon.desktop
