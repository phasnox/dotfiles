#!/bin/sh
curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
fish -c "set fish_user_paths ~/.local/bin"
fish -c "fisher install edc/bass matchai/spacefish jethrokuan/fzf danhper/fish-ssh-agent"
