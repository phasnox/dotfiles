#!/bin/sh

# chromium --ozone-platform=wayland --use-cmd-decoder=passthrough --gtk-version=4 $@
 chromium --enable-features=VaapiVideoDecoder --disable-features=UseChromeOSDirectVideoDecoder --ignore-gpu-blocklist --enable-gpu-rasterization --enable-zero-copy --canvas-oop-rasterization --ozone-platform=wayland $@
