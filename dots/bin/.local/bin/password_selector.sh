#!/bin/bash

export BW_SESSION=$(cat ~/.bwsession)

# Combine the names and IDs into a single string, separated by newlines
data=$(
  bw list items --pretty --session $BW_SESSION | \
  jq -r '.[] | .id + " " + .name + " " + .login.username + " " + .login.password'
)

# Use `fzf` to display a fuzzy search interface and select a password item
# selected=$(echo "$data" | fzf --with-nth 2..3 --preview "bw get item {1} --session $BW_SESSION | jq ." --preview-window=up:50%:wrap)
selected=$(
  echo "$data" | \
  fzf --with-nth 2..3
)
# selected=$(echo "$data" | rofi -dmenu)

# Extract the selected item ID
# selected_id=$(echo $selected | awk '{print $1}')
echo $selected | awk '{print $4}' | nohup xclip -loop 0 -sel clipboard >/dev/null 2>&1

# Use `bw get item` command to retrieve the password for the selected item
# bw get item "$selected_id" --raw --session $BW_SESSION | jq -r .login.password | nohup xclip -loop 0 -sel clipboard >/dev/null 2>&1
