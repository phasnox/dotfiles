function bspc_is_monocle() {
  LAYOUT=$(bspc query -T -d | jq .layout -r)
  if [[ "$LAYOUT" == "monocle" ]]; then
    true
  else
    false
  fi
}

function bspc_toggle_hidden() {
  for node_i in $(bspc query -N -n '.!focused.window' -d focused);  do
    bspc node $node_i -g hidden;
  done
}

function bspc_toggle_hidden_node() {
  if [[ "$1" == "hide" ]]; then
    bspc node -g hidden;
  else
    node=$(bspc wm -d | jq -r .focusHistory[-2].nodeId)
    bspc node $node -g hidden;
  fi
}

function bspc_next_node() {
  if bspc_is_monocle; then
    bspc_toggle_hidden;
  fi
  bspc node -f next.local.window
  if bspc_is_monocle; then
    bspc_toggle_hidden;
  fi
}

function bspc_prev_node() {
  if bspc_is_monocle; then
    bspc_toggle_hidden;
  fi
  bspc node -f prev.local.window
  if bspc_is_monocle; then
    bspc_toggle_hidden;
  fi
}

function bspc_hide_all() {
  for node_i in $(bspc query -N -n '.!focused.window' -d focused);  do
    bspc node $node_i --flag hidden=on;
  done
}

function bspc_show_all() {
  for node_i in $(bspc query -N -n '.!focused.window' -d focused);  do
    bspc node $node_i --flag hidden=off;
  done
}

function bspc_next_layout() {
  bspc desktop -l next;
  if bspc_is_monocle; then
    echo hide
    bspc_hide_all;
  else
    echo show
    bspc_show_all
  fi
}
