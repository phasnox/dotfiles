" vim: set fdm=marker:

set nocompatible              " be iMproved, required
filetype off                  " required


" Plugins {{{

" let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
" if empty(glob(data_dir . '/autoload/plug.vim'))
"   silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
"   autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
" endif
"
" call plug#begin('~/.vim/plugged')
" Plug 'Townk/vim-autoclose'
" Plug 'scrooloose/nerdtree'
" Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" Plug 'junegunn/fzf.vim'
"
" " Git support
" Plug 'tpope/vim-fugitive'
" Plug 'tommcdo/vim-fubitive'
" Plug 'tpope/vim-rhubarb'
" Plug 'shumphrey/fugitive-gitlab.vim'
"
" Plug 'tpope/vim-surround'
" Plug 'tpope/vim-repeat'
" Plug 'tpope/vim-abolish'
" Plug 'pangloss/vim-javascript'
" Plug 'sjl/gundo.vim'
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'ryanoasis/vim-devicons'
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
" Plug 'preservim/nerdcommenter'
" Plug 'morhetz/gruvbox'
" Plug 'joshdick/onedark.vim'
" Plug 'elixir-editors/vim-elixir'
" Plug 'airblade/vim-rooter'
"Plug 'mxw/vim-jsx'
"Plug 'xolox/vim-colorscheme-switcher'
"Plug 'xolox/vim-misc'

" call plug#end()

" }}}

" Plugin Options {{{
" Nerd Tree
" let NERDTreeIgnore = ['\.pyc$', '__pycache__', 'node_modules']

" Vim rooter
" let g:rooter_patterns = ['mix.exs', 'Rakefile', '.git/']

"FZF Options
" let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }
" }}}

" Vim Options{{{
set termguicolors
set cursorcolumn
set cursorline
filetype plugin indent on    " required

" Automatic .vimrc reloading
autocmd! bufwritepost ~/.vimrc source % 
autocmd! bufwritepost ~/.vimrc set fdm=marker

let mapleader="<space>"
syntax on
set nocompatible

" Exclude files 
set wildignore+=*.png,*.jpg,*.o,*.pyc,*node_modules/**,*bower_components/**,.git/**,venv/**

" Vertical diff split
set diffopt=vertical

" Better copy paste
set pastetoggle=<F2>
set clipboard+=unnamedplus

set autoindent
set backspace=2
set cindent
set number
set relativenumber
set smartindent

" Search helpers
set hlsearch
set showcmd            " Show (partial) command in status line.
set showmatch          " Show matching brackets.
set ignorecase         " Do case insensitive matching
set smartcase          " Do smart case matching
set incsearch          " Incremental search
set autowrite          " Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
set mouse=a            " Enable mouse usage (all modes)

" swap file disable
set nobackup
set nowritebackup
set noswapfile

" Indentation issues
set tabstop=8
set expandtab
set softtabstop=4
set shiftwidth=4

set tw=79          " width del documento
set nowrap         " no auto wrap on load
set fo-=t          " no auto wrap when typing
set colorcolumn=80
let &colorcolumn=join(range(81,999),",")

" Have Vim jump to the last position when reopening a file
"if has("autocmd")
"    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Folding code
"---------------
set foldmethod=syntax
set foldlevelstart=2
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent! loadview 
"}}}

" Au Commands {{{
"Autoclose comment buffer
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" File type definitions
"autocmd BufNewFile,BufRead *.json,*.jsx,*.babelrc set ft=javascript

" Filetype tabs
" ==================================
autocmd Filetype html setlocal ts=2 sts=2 sw=2
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
autocmd Filetype ruby setlocal ts=2 sts=2 sw=2
autocmd Filetype css setlocal ts=2 sts=2 sw=2
autocmd Filetype scss setlocal ts=2 sts=2 sw=2
autocmd Filetype yaml setlocal ts=2 sts=2 sw=2
autocmd Filetype elixir setlocal ts=2 sts=2 sw=2
autocmd Filetype eelixir setlocal ts=2 sts=2 sw=2
autocmd Filetype eelixir setlocal syntax=html
autocmd FileType c,cpp,python,ruby,java,javascript,elixir,eelixir
            \ autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType c,cpp,python,ruby,java,javascript,elixir,eelixir
            \ autocmd BufWritePre <buffer> :silent g/^[\n]$/d
" }}}

" Key Mappings {{{

" NerdTree
" map <leader>nf :NERDTreeFind<CR>
" map <leader>nt :NERDTreeToggle<CR>

" Navigate through buffers
noremap <leader>b :bn<CR>
noremap <leader>B :bn<CR>

" Fugitive
" noremap <leader>gs :G<CR>
" noremap <leader>gt :diffget //2<CR>
" noremap <leader>gn :diffget //3<CR>

" Ver la historia de un archivo
" map <leader>h :GundoToggle<CR>

" format JSON
map <leader>j !python -m json.tool<CR>

" format JSON
map <leader>js !js-beautify -s 2 --brace-style=expand -i<CR>

" Make
noremap <leader>m :make! %<CR>
noremap <leader>ma :make!<CR>

" Copy file path to the clipboard
noremap <leader>p :let @+=@%<CR>

" FuzzySearch
" map <leader>r :Rg<CR>
" map <leader>s :FZF<CR>
" map <leader>f :FZF<CR>

" Tab navigation
noremap <leader>t gt
noremap <leader>T gT

noremap <leader>w :wa<CR>

" Navigate through splits
map <c-left> <c-w>h
map <c-down> <c-w>j
map <c-up> <c-w>k
map <c-right> <c-w>l


" Semicolon shortcut
noremap ; :
vnoremap ; :

" }}}

" Misc {{{
" Clear search highlights
noremap <silent><Leader>/ :nohls<CR>

let g:ruby_path = system('echo $HOME/.rbenv/shims')
" Regex Engine
set re=1
" }}}

" UltiSnips {{{
" let g:UltiSnipsExpandTrigger=",c"
" let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']
"}}}

" Colorscheme {{{
"if strftime("%H") < 13
" if strftime("%m") % 2 != 0
"   set background=dark
"   " colorscheme gruvbox
"   colorscheme summerfruit256
" else
"   " onedark.vim override: Don't set a background color when running in a terminal;
"   " just use the terminal's background color
"   " `gui` is the hex color code used in GUI mode/nvim true-color mode
"   " `cterm` is the color code used in 256-color mode
"   " `cterm16` is the color code used in 16-color mode
"   if (has("autocmd") && !has("gui_running"))
"     augroup colorset
"       autocmd!
"       let s:white = { "gui": "#ABB2BF", "cterm": "145", "cterm16" : "7" }
"       autocmd ColorScheme * call onedark#set_highlight("Normal", { "fg": s:white }) " `bg` will not be styled since there is no `bg` setting
"     augroup END
"   endif
"   set background=dark
"   " colorscheme onedark
"   "colorscheme gruvbox
"   colorscheme phasma2
"   "colorscheme spaceway
" endif
" }}}

set background=dark
"colorscheme gruvbox
colorscheme phasma2

" Make au {{{
au BufEnter 
\ *.{exs,ex}
\ setlocal makeprg=mix\ test

au BufEnter 
\ *.sh
\ setlocal makeprg=sh
" }}}

" TabLine {{{
function MyTabLine()
    let cwd = split(getcwd(), '/')
    let s = '%#Directory#[' . cwd[-1] . '] '

    let mbranch = '%{FugitiveHead()}'
    if !empty(mbranch)
        let s .= '%#Number#[' . mbranch . '] '
    endif 

    for i in range(tabpagenr('$'))
        "select the highlighting
        if i + 1 == tabpagenr()
            let s .= '%#TabLineSel#'
        else
            let s .= '%#TabLine#'
        endif

         "set the tab page number (for mouse clicks)
        let s .= '%' . (i + 1) . 'T'

         "the label is made by MyTabLabel()
        let s .= ' %{MyTabLabel(' . (i + 1) . ')} '
    endfor

     "after the last tab fill with TabLineFill and reset tab page nr
    let s .= '%#TabLineFill#%T'

     "right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let s .= '%=%#TabLine#%999Xclose'
    endif

    return s
endfunction

function MyTabLabel(n)
    let buflist = tabpagebuflist(a:n)
    let winnr = tabpagewinnr(a:n)
    let tabs = split(bufname(buflist[winnr - 1]), '/')
    if empty(tabs)
        return '(No name)'
    else
        return tabs[-1]
    endif
endfunction

set tabline=%!MyTabLine()
" }}}
