local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.git_files, {})
vim.keymap.set('n', '<leader>ft', builtin.live_grep, {})
-- vim.keymap.set('n', '<leader>ft', function()
-- 	builtin.grep_string({ search = vim.fn.input("Grep > ") })
-- end)
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fo', builtin.oldfiles, {})
vim.keymap.set('n', '<leader>o', builtin.oldfiles, {})

require('telescope').setup{
  defaults = {
    mappings = {
      n = {
    	  ['dd'] = require('telescope.actions').delete_buffer
      }, -- n
    } -- mappings
  }, -- defaults
} -- telescope setup
