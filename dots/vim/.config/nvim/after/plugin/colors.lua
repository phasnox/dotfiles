require('rose-pine').setup({
    disable_background = true
})

function ColorMyPencils(color)
	-- color = color or "rose-pine-dawn"
	color = color or "everforest"
	vim.cmd.colorscheme(color)
  vim.cmd.set("background=dark")

	-- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
	-- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })

end

ColorMyPencils("everforest")
