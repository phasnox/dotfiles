function! test#elixirscript#elixirscriptrunner#test_file(file) abort
  return a:file == "/tmp/test.exs" || a:file == "/tmp/t.exs"
endfunction

function! test#elixirscript#elixirscriptrunner#build_position(type, position) abort
  return [a:position['file']]
endfunction

function! test#elixirscript#elixirscriptrunner#build_args(args) abort
 return a:args
endfunction

function! test#elixirscript#elixirscriptrunner#executable() abort
    return 'iex --dot-iex '
endfunction
