require("phasnox.set")
require("phasnox.remap")

local augroup = vim.api.nvim_create_augroup
local trailingSpaces = augroup('TrailingSpaces', {})

local autocmd = vim.api.nvim_create_autocmd
local yank_group = augroup('HighlightYank', {})

function R(name)
    require("plenary.reload").reload_module(name)
end

autocmd('TextYankPost', {
    group = yank_group,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 40,
        })
    end,
})

autocmd({"BufWritePre"}, {
    group = trailingSpaces,
    pattern = "*",
    command = [[%s/\s\+$//e]],
})

autocmd({"BufRead","BufNewFile"}, {
    pattern = "*Notes/zett*",
    command = "setlocal wrap linebreak nolist",
})

autocmd({"BufRead","BufNewFile"}, {
    pattern = "*Notes/zett*",
    command = "cd ~/Notes/",
})

autocmd({"BufRead","BufNewFile"}, {
    pattern = "*dotfiles*",
    command = "cd ~/.dotfiles",
})

-- Restore cursor position
vim.api.nvim_create_autocmd({ "BufReadPost" }, {
    pattern = { "*" },
    callback = function()
        vim.api.nvim_exec('silent! normal! g`"zv', false)
    end,
})

vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
