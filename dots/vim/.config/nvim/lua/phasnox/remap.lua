vim.g.mapleader = " "
vim.keymap.set("i", "jk", "<Esc>")
vim.keymap.set("n", ";", ":")
vim.keymap.set("n", "<leader>s", "<C-w>s")
vim.keymap.set("n", "<leader>v", "<C-w>v")

-- Move lines in visual mode
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Search and center
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Save
vim.keymap.set("n", "<leader>w", ":wall<CR>")

vim.keymap.set("n", "Q", "<nop>")

-- Fun
vim.keymap.set("n", "<leader>mr", "<cmd>CellularAutomaton make_it_rain<CR>");

-- Scroll down/up
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- Move in splits
vim.keymap.set("n", "<C-h>", "<C-w><left>")
vim.keymap.set("n", "<C-j>", "<C-w><down>")
vim.keymap.set("n", "<C-k>", "<C-w><up>")
vim.keymap.set("n", "<C-l>", "<C-w><right>")

-- Move through buffers
vim.keymap.set("n", "<M-j>", "<cmd>bnext<CR>")
vim.keymap.set("n", "<M-k>", "<cmd>bprev<CR>")

-- Run tests
vim.cmd "let g:test#strategy = 'toggleterm'"
vim.keymap.set("n", ",m", ":wall<cr>:TestNearest<cr>")
vim.keymap.set("n", ",M", ":wall<cr>:TestFile<cr>")

-- Review
-- ---------------------
vim.keymap.set("n", "<leader>e", vim.cmd.Ex)

--
-- greatest remap ever
vim.keymap.set({ "n", "v" }, "<leader>p", [["+p]])
vim.keymap.set({ "n", "v" }, "<leader>P", [["+P]])
vim.keymap.set("n", "gp", [["_dp]])

vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])

-- This navigates through errors, problem is it conflicts with split navigation
-- vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
-- vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

