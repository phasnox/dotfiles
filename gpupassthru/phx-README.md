## Links for fedora

#### GPU complete guide
https://www.youtube.com/watch?v=KVDUs019IB8

### Load vfio-pci early and rebuild the initramfs
Garuda uses dracut:

```
sudo cp 10-vfio.conf /etc/dracut.conf.d/
dracut --hostonly --no-hostonly-cmdline /boot/initramfs-linux.img
```

#### USB Passthrough
https://www.youtube.com/watch?v=IYOmuPzrdXk

#### For 127 error that prevents from reruning the vm without reboot
https://github.com/gnif/vendor-reset

#### Vendor reset not working

When the GPU stops working after a restart.
https://github.com/gnif/vendor-reset/issues/46#issuecomment-1013220284

#### Check for kernel options needed
```
sudo grep -E 'CONFIG_FTRACE|CONFIG_KPROBES|CONFIG_PCI_QUIRKS|CONFIG_KALLSYMS|CONFIG_KALLSYMS_ALL|CONFIG_FUNCTION_TRACER' /bo
ot/config-$(uname -r)
```
