#!/bin/bash

source ./scripts/helpers.sh

distro=$(get_distro)

echo "###########################"
echo "# Installing on $distro.. #"
echo "###########################"
echo ""

if [ "$distro" == "" ]; then
    echo "Unknown distro.."
    exit 1;
else
    echo "[+] Installing packages..";
fi

if [ "$distro" == "debian" ]; then
    sh ./scripts/debian_install.sh
fi

if [ "$distro" == "fedora" ]; then
    sh ./scripts/fedora_install.sh
fi

if [ "$distro" == "arch" ]; then
    sh ./scripts/arch_install.sh
fi

if [ "$distro" == "kali" ]; then
    sh ./scripts/debian_install.sh
fi


echo "[+] Installing fonts.."
sh ./scripts/fonts_install.sh

#echo "[+] Installing vim-plug.."
#sh ./scripts/vim_install.sh

echo "[+] Installing fish stuff.."
sh ./scripts/fish_install.sh

echo "[+] Install asdf.."
bash ./scripts/asdf_install.sh

echo "[+] Install python local.."
bash ./scripts/python_install.sh

echo "[+] Installing dotfiles.."
sh ./scripts/dots_install.sh

echo "[+] Installing language servers dependencies.."
sudo npm install -g typescript typescript-language-server ls_emmet

toilet -F metal =TODO=
echo ""
echo "[ ] Put email in ~/.gitconfig"
