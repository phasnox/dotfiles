## List of most common vim actions

- Slack scratchpad
- Zoom scratchpad
x Mouse natural scroll
x keyboard ngr keys
x Screenshot tool
x Search file ff
x Search text ft
x Split window s horizontal, v vertical
x Move between splits
x Comment line
x Make test
x Toggle terminal, use toggleterm? Or builtin
x Rename variable lead>crn
x Format file lead>cfmt
- Toggle file tree
- Duplicate file
- Save a mark on test file and exec on shortcut
-

---
## Maybe
- Navigate through errors


## TODO
x Delete buffer with dd in normal mode when in telescope buffers
x Move between buffers shortcut
- Remove toggleterm, use custom fn?
- LuaSnips
- Check elixir and eex.html files
- Check elixir snippet working
- Check .envrc .env comment line working
- Fugitive config/use
- Copilot/tabine?
