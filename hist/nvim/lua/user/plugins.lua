local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
---@diagnostic disable-next-line: missing-parameter
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  -- snapshot = "july-24",
  snapshot_path = fn.stdpath "config" .. "/snapshots",
  max_jobs = 50,
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
    prompt_border = "rounded", -- Border style of prompt popups.
  },
}

-- Install your plugins here
return packer.startup(function(use)
  -- Plugin Mangager
  use "wbthomason/packer.nvim" -- Have packer manage itself

  -- use "dstein64/vim-startuptime" -- Startuptime plugin

  -- Lua Development
  use "nvim-lua/plenary.nvim" -- Useful lua functions used ny lots of plugins
  use "nvim-lua/popup.nvim"
  use "christianchiarulli/lua-dev.nvim"
  -- use "folke/lua-dev.nvim"

  -- LSP
  use "neovim/nvim-lspconfig" -- enable LSP
  use "williamboman/mason.nvim"
  use "williamboman/mason-lspconfig.nvim"
  use "jose-elias-alvarez/null-ls.nvim" -- for formatters and linters
  use "ray-x/lsp_signature.nvim"
  use "SmiteshP/nvim-navic"
  use "simrat39/symbols-outline.nvim"
  use "b0o/SchemaStore.nvim"
  -- use "github/copilot.vim"
  -- use {
  --   "zbirenbaum/copilot.lua",
  --   event = { "VimEnter" },
  --   config = function()
  --     vim.defer_fn(function()
  --       require "user.copilot"
  --     end, 100)
  --   end,
  -- }
  use "RRethy/vim-illuminate"
  use "j-hui/fidget.nvim"
  -- use "lvimuser/lsp-inlayhints.nvim"
  use "https://git.sr.ht/~whynothugo/lsp_lines.nvim"

  -- Completion
  use "christianchiarulli/nvim-cmp"
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions
  use "hrsh7th/cmp-nvim-lsp"
  use "hrsh7th/cmp-emoji"
  use "hrsh7th/cmp-nvim-lua"
  -- use "zbirenbaum/copilot-cmp"
--   use { "tzachar/cmp-tabnine", commit = "1a8fd2795e4317fd564da269cc64a2fa17ee854e", 
-- run = "./install.sh" }

  -- Snippet
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- Elixir
  use "elixir-editors/vim-elixir"

  -- Syntax/Treesitter
  -- ===================
  use "nvim-treesitter/nvim-treesitter"
  -- View treesitter info directly in the editor
  -- use "nvim-treesitter/playground"

  -- Autocloses html tags
  use "windwp/nvim-ts-autotag"

  -- Allows to define text objects. e.g. <normalmode>yif yanks "in" function
  -- See treesitter.lua > textobjects
  use "nvim-treesitter/nvim-treesitter-textobjects"

  -- Like tpope surround
  -- use "kylechui/nvim-surround"

  -- Marks
  use "christianchiarulli/harpoon"
  use "MattesGroeger/vim-bookmarks"

  -- Fuzzy Finder/Telescope
  use "nvim-telescope/telescope.nvim"
  -- use "nvim-telescope/telescope-media-files.nvim"
  use "tom-anders/telescope-vim-bookmarks.nvim"

  -- Note Taking
  use "mickael-menu/zk-nvim"

  -- Color
  use "NvChad/nvim-colorizer.lua"
  -- use "ziontee113/color-picker.nvim"
  use "nvim-colortils/colortils.nvim"

  -- Colorschemes
  use "lunarvim/onedarker.nvim"
  use "lunarvim/darkplus.nvim"
  -- use "folke/tokyonight.nvim"
  -- use "lunarvim/colorschemes"

  -- Utility
  use "rcarriga/nvim-notify"
  use "stevearc/dressing.nvim"
  use "ghillb/cybu.nvim"
  use "lewis6991/impatient.nvim"
  -- use "lalitmee/browse.nvim"

  -- Registers
  -- use "tversteeg/registers.nvim"

  -- Icon
  use "kyazdani42/nvim-web-devicons"

  -- Debugging
  use "mfussenegger/nvim-dap"
  use "rcarriga/nvim-dap-ui"
  -- use "theHamsta/nvim-dap-virtual-text"
  -- use "Pocco81/DAPInstall.nvim"

  -- Tabline
  -- use "akinsho/bufferline.nvim"
  -- use "tiagovla/scope.nvim"

  -- Statusline
  use "christianchiarulli/lualine.nvim"

  -- Indent
  use "lukas-reineke/indent-blankline.nvim"

  -- File Explorer
  use "kyazdani42/nvim-tree.lua"
  use "christianchiarulli/lir.nvim"

  -- Comment
  use "numToStr/Comment.nvim"
  use "folke/todo-comments.nvim"

  -- Terminal
  use "akinsho/toggleterm.nvim"

  -- Project
  use "ahmedkhalf/project.nvim"
  use "windwp/nvim-spectre"

  -- Session
  -- use "rmagatti/auto-session"
  -- use "rmagatti/session-lens"

  -- Quickfix
  use "kevinhwang91/nvim-bqf" -- TODO LEARN phasnox

  -- Code Runner
  -- TODO Maybe phasnox
  -- use {
  --   "0x100101/lab.nvim",
  --   run = "cd js && npm ci",
  -- }

  -- Tests
  use "vim-test/vim-test"

  -- Git
  use "lewis6991/gitsigns.nvim"
  use "f-person/git-blame.nvim"
  use "tpope/vim-fugitive"
  use "tpope/vim-rhubarb"
  -- use "ruifm/gitlinker.nvim"
  -- use "mattn/vim-gist" -- TODO Maybe phasnox
  -- use "mattn/webapi-vim" -- TODO ??? phasnox

  -- Editing Support
  use "windwp/nvim-autopairs"
  -- use "monaqa/dial.nvim" -- TODO Maybe phasnox
  use "nacro90/numb.nvim" -- Goto line by :linenum 
  use "andymass/vim-matchup" -- TODO LEARN phasnox
  -- use "folke/zen-mode.nvim" -- TODO Maybe phasnox
  -- use "karb94/neoscroll.nvim" -- TODO Maybe phasnox
  use "junegunn/vim-slash" -- Better / buffer search - phasnox

  -- Motion
  -- TODO LEARN phasnox
  -- use "phaazon/hop.nvim"
  -- use "jinh0/eyeliner.nvim"

  -- Keybinding
  use "folke/which-key.nvim"

  -- Rust
  -- TODO Maybe phasnox
  -- use { "christianchiarulli/rust-tools.nvim", branch = "modularize_and_inlay_rewrite" }
  -- use "Saecki/crates.nvim"

  -- Typescript TODO: set this up, also add keybinds to ftplugin
  -- TODO Maybe phasnox
  -- use "jose-elias-alvarez/typescript.nvim"

  -- Markdown
  -- TODO Maybe phasnox
  -- use {
  --   "iamcco/markdown-preview.nvim",
  --   run = "cd app && npm install",
  --   ft = "markdown",
  -- }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
