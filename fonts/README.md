# FONTS

## Install fontforge
```
dnf install fontforge
```

## Patch font
## e.g.
```
fontforge -script font-patcher --glyphdir ./ --custom fa5ps.otf terminus.ttf
````
